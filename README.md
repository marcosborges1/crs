# Cloud Restriction Solver (CRS)

Cloud Restriction Solver (CRS) is semiautomatic approach for migrating applications to a PaaS environment that avoids the cloud restrictions through user-defined refactorings. The approach, which fosters software reuse and is cloud-independent, consists of two phases: restriction identification, identifies the pieces of code that violate the restrictions of the chosen PaaS platform, and refactoring execution, changes those pieces by equivalent cloud enabled services. The phases are supported by open and extensible engines, CRSAnlayer and CRSRefactor which constitute the CRS framework, available in this repository, that implements the approach.

CRS is based on AutoRefactor, described below.


## AutoRefactor

The AutoRefactor project delivers free software that automatically refactor code bases.

The aim is to fix language/API usage in order to deliver smaller, more maintainable and more expressive code bases.

This is an Eclipse plugin to automatically refactor Java code bases.

You will find much more information on [http://autorefactor.org](http://autorefactor.org): goals, features, usage, samples, installation, links.

## License

AutoRefactor is distributed under the terms of both the
Eclipse Public License v1.0 and the GNU GPLv3+.

See LICENSE-ECLIPSE, LICENSE-GNUGPL, and COPYRIGHT for details.