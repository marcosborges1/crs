package org.autorefactor.analyzer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class PaaS {

	protected String abbreviation,description;
	protected HashMap<String, List<DetectionTypes>> listRestrictedClasses;

//	public enum DetectionTypes {
//		instanciation, declaration, extension, implementation
//	}

	public List<DetectionTypes> addDetections(DetectionTypes... detection) {
		List<DetectionTypes> detectionTypes = new ArrayList<DetectionTypes>();
		for (DetectionTypes d : detection) {
			detectionTypes.add(d);
		}
		return detectionTypes;
	}

	public abstract void registerRestritedClasses();

	public HashMap<String, List<DetectionTypes>> getListRestrictedClasses() {
		return listRestrictedClasses;
	}
	public boolean isPresentInRestritedClasses(String classFullName) {
		return listRestrictedClasses.containsKey(classFullName);
	}

}
