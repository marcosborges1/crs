/*
 * AutoRefactor - Eclipse plugin to automatically refactor Java code bases.
 *
 * Copyright (C) 2014-2016 Jean-Noël Rouvignac - initial API and implementation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program under LICENSE-GNUGPL.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution under LICENSE-ECLIPSE, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.autorefactor.analyzer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.autorefactor.AutoRefactorPlugin;
import org.autorefactor.preferences.Preferences;
import org.autorefactor.analyzer.AnalyzerRule;

/**
 * Lists all the available analyzer rules.
 */
public final class AllAnalyzersRules {

	private AllAnalyzersRules() {
		super();
	}

	/**
	 * Returns the analyzer rules which have been enabled from the Eclipse
	 * preferences.
	 *
	 * @return the analyzer rules which have been enabled from the Eclipse
	 *         preferences
	 */
	public static List<AnalyzerRule> getConfiguredAnalyzerRules() {
		final Preferences prefs = AutoRefactorPlugin.getPreferenceHelper();
		final List<AnalyzerRule> refactorings = getAllAnalyzersRules();
		for (final Iterator<AnalyzerRule> iter = refactorings.iterator(); iter.hasNext();) {
			final AnalyzerRule analyzer = iter.next();
			if (!analyzer.isEnabled(prefs)) {
				iter.remove();
			}
		}
		return refactorings;
	}

	/**
	 * Returns all the available analyzer rules.
	 *
	 * @return all the available analyzer rules
	 */
	public static List<AnalyzerRule> getAllAnalyzersRules() {
		return newArrayList(new CRSAnalyzer());
	}

	private static List<AnalyzerRule> newArrayList(AnalyzerRule... analyzers) {
		final List<AnalyzerRule> results = new ArrayList<AnalyzerRule>(analyzers.length);
		for (AnalyzerRule r : analyzers) {
			if (r != null) {
				results.add(r);
			}
		}
		return results;
	}

}
