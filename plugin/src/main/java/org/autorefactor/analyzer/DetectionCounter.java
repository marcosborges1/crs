package org.autorefactor.analyzer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.autorefactor.analyzer.PaaS.DetectionTypes;

public class DetectionCounter {

	public static DetectionCounter detectionCounter;
	Map<List<String>, Integer> counter = new HashMap<List<String>, Integer>();
	JSONAnalyzer jsonAnalyzer2 = new JSONAnalyzer();

	private static DetectionCounter getDetectionCounter() {
		return detectionCounter;
	}

	public static void setDetectionCounter(DetectionCounter detectionCounter) {
		DetectionCounter.detectionCounter = detectionCounter;
	}

	protected DetectionCounter() {
	}

	public static DetectionCounter getInstance() {
		if (DetectionCounter.getDetectionCounter() == null)
			DetectionCounter.setDetectionCounter(new DetectionCounter());
		return DetectionCounter.getDetectionCounter();
	}

	synchronized public void countOccurrences(DetectionTypes currentDetection, String restrictedClass) {
		
		int count;
		if (counter.get(Arrays.asList(currentDetection.toString(), restrictedClass)) == null) {
			count = 1;
		} 
		else {
//			System.err.println("Antes"+getLastClount());
			count =  counter.get(Arrays.asList(currentDetection.toString(), restrictedClass));
			count++;
		}		
		counter.put(Arrays.asList(currentDetection.toString(), restrictedClass.toString()), count);
		System.err.println(Arrays.asList(currentDetection.toString(), restrictedClass)+ "/" + count);
		
//		int count;
//		if (jsonAnalyzer2.getObjects(currentDetection.toString(), restrictedClass)==null) {
//			count = 1;
//			JSONObject[] arrayJSONObjects = jsonAnalyzer2.createObjects(jsonAnalyzer2, currentDetection.toString(), restrictedClass);
//			arrayJSONObjects[1].put(restrictedClass, new Integer(count));
////			System.err.println(restrictedClass);
//			System.err.println(jsonAnalyzer2);
//			
//		} else {
//			
//			System.err.println(jsonAnalyzer2.getObjects(currentDetection.toString(), restrictedClass));
////			count = counter.get(Arrays.asList(restrictedClass, currentDetection.toString()));
//			//count++;
////			System.out.println(currentDetection.toString());
////			System.err.println(jsonAnalyzer2.getObjects(currentDetection.toString(), restrictedClass));
//
//		}
	
	}
	
	public void showReport() {
		for (Map.Entry<List<String>, Integer> entry : counter.entrySet())
		{
//			System.err.println(entry.getKey() + "/" + entry.getValue());
//		    JSONObject[] arrayJSONObjects = jsonAnalyzer2.createObjects(jsonAnalyzer2, entry.getKey().get(0), entry.getKey().get(1));
//		    arrayJSONObjects[1].put(entry.getKey().get(1), new Integer(entry.getValue()));
		}
//		System.err.println(jsonAnalyzer2);
//		try {
////			saveRegister(jsonAnalyzer2);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}
	private void saveRegister(JSONAnalyzer json) throws IOException {
		String fileToSave = System.getProperty("user.home") + "/crsanalyzer/counter.json";
		File file = new File(fileToSave);
		file.getParentFile().mkdirs();
		file.createNewFile();

		FileWriter fileW = new FileWriter(fileToSave);
		fileW.write(json.toJSONString());
		fileW.flush();
		fileW.close();
		// System.err.println("Result"+json.toJSONString());

	}

}
